<?php

/* @var $this yii\web\View */
use yii\widgets\ActiveForm;
use yii\helpers\Html;

$this->title = 'My Yii Application';
?>
<div class="site-index">

<?php
    if (Yii::$app->request->get('success') == 1){
        echo '<div class="alert alert-success" role="alert">Your data has been successfully saved.</div>';
    }
?>

<?php $form = ActiveForm::begin(['options'=>['class'=>'form-inline', 'style'=>'float: left; margin-bottom:10px']]); ?>
<div class="form-group">
    <input type="text" id="tag" name="tag" placeholder="Search tags.." class="form-control">
    <input type="submit" id="btnSearch" value="Search"class="btn btn-default">
</div>
<?php ActiveForm::end(); ?>
<?php
if (isset($instas) && count($instas) > 0){
    echo Html::beginForm(yii\helpers\Url::toRoute('site/save'), 'POST', ['id'=>'w1']);
?>
    <input type="submit" id="btnSave" value="Save"class="btn btn-success" style="margin-left:3px">

<hr style="clear: both; margin-top:10px">

    <div class="row" id="div-instagram">

<?php
        foreach($instas as $item){
?>
        <div class="col-sm-6 col-md-4" style="word-wrap: break-word;">
            <div class="thumbnail">
                <input type="hidden" value="<?= $item["id"] ?>" name="insta_id[]">
                <a href="<?= $item["link"] ?>"><img src="<?= $item["images"]["low_resolution"]["url"] ?>"></a>
                <div class="caption">
                    <h3>@<?= $item["user"]["username"] ?></h3>
                    <p style="height:80px; overflow:hidden"><?= $item["caption"] ?></p>
                </div>
                <div class="caption" style="padding-top:0px; margin-top:-8px">
                    <i class="glyphicon glyphicon-thumbs-up"></i> <?= $item["likes"] ?>
                </div>
            </div>
        </div>
<?php } ?>
<?php
    echo Html::endForm();
}
?>

    
</div>


<style>
    .thumbnail{
        height:510px;
    }
</style>