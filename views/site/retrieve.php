<?php

/* @var $this yii\web\View */
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\widgets\ListView;

$this->title = 'My Yii Application';
?>
<div class="site-index">
    
    <?php //var_dump($dataProvider) ?>

    <?= ListView::widget([
        'layout' => "{sorter}\n{summary}\n{items}\n<div style='clear:both'>{pager}</div>",
        'dataProvider' => $dataProvider,
        'itemOptions' => ['class' => 'item'],
        'itemView' => function ($item, $key, $index, $widget) {
                $str = nl2br($item["caption"]);
                if (strlen($str) > 200){
                    $str = substr($str, 0, 197) . '...';   
                }
                return '<div class="col-sm-6 col-md-4" style="word-wrap: break-word;">
                    <div class="thumbnail">
                        <input type="hidden" value="'.$item["id"].'" name="insta_id[]">
                        <a href="'.$item["link"].'"><img src="'.$item["images"]["low_resolution"]["url"].'"></a>
                        <div class="caption">
                            <h3>@'.$item["user"]["username"].'</h3>
                            <p style="height:80px; overflow:hidden">'.$str.'</p>
                        </div>
                        <div class="caption" style="padding-top:0px; margin-top:-8px">
                            <i class="glyphicon glyphicon-thumbs-up"></i>'.$item["likes"].'
                        </div>
                    </div>
                </div>';
            },
    ]) ?>
    
</div>


<style>
    .thumbnail{
        height:510px;
    }
</style>