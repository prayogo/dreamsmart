<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\mongodb\Connection;
use yii\mongodb\Query;
use yii\data\ActiveDataProvider;

class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        $output = [];
        if (isset($_POST["tag"]) && $_POST["tag"] != ""){

            $tag = $_POST['tag'];
            $client_id = "de5669e323bd478598cec88a29517ce7";

            $url = 'https://api.instagram.com/v1/tags/'.$tag.'/media/recent?client_id='.$client_id;

            $inst_stream = $this->callInstagram($url);
            $results = json_decode($inst_stream, true);

            foreach($results['data'] as $item){
                $str = nl2br($item["caption"]["text"]);
                if (strlen($str) > 200){
                    $str = substr($str, 0, 197) . '...';   
                }
                array_push($output, [
                    'id' => $item["id"],
                    'type' => $item["type"],
                    'tags' => implode("#",$item["tags"]),
                    'caption' => $str,
                    'likes' => $item["likes"]["count"],
                    'link' => $item["link"],
                    'user' => $item["user"],
                    'images' => $item["images"],
                    'id' => $item["id"],
                ]);
            }

            return $this->render('index', [
                'instas' => $output,
            ]);
    
        }else{
            return $this->render('index', [
                'instas' => $output,
            ]);   
        }
    }

    public function actionSave()
    {
        if (isset($_POST["insta_id"])){
            foreach($_POST["insta_id"] as $id){
                $client_id = "de5669e323bd478598cec88a29517ce7";

                $url = 'https://api.instagram.com/v1/media/'.$id.'?client_id='.$client_id;

                $inst_stream = $this->callInstagram($url);
                $results = json_decode($inst_stream, true);

                $item = $results["data"];
                $database = Yii::$app->mongodb->getDatabase('dreamsmart');
                $collection = Yii::$app->mongodb->getCollection('instagram');
                $collection->insert([
                    'id' => $item["id"],
                    'type' => $item["type"],
                    'tags' => implode("#",$item["tags"]),
                    'caption' => $item["caption"]["text"],
                    'likes' => $item["likes"]["count"],
                    'link' => $item["link"],
                    'user' => $item["user"],
                    'images' => $item["images"],
                    'id' => $item["id"],
                ]);

            }
            return $this->redirect(['index', 
                'instas' => null,
                'success' => 1
            ]);
        }
    }

    public function actionRetrieve(){
        $params = Yii::$app->request->queryParams;
        $query = new Query;
        $size = 12;

        $query->select([])
            ->from('instagram')
            ->orderBy('tags');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => array('pageSize' => $size),
        ]);

        return $this->render('retrieve', [
            'dataProvider' => $dataProvider,
        ]);
    }

    function callInstagram($url)
    {
        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => 2
        ));

        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }
}
